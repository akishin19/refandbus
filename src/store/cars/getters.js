const CARS_IN_GRID = 5;

export const getCarById = (state) => (id) => {
	return state.carsList.find((car) => car.id === id);
};

export const newestCarsList = (state) => {
	const carsList = [...state.carsList];

	carsList.sort((firstCar, secondCar) => {
		return (
			new Date(secondCar.creationDate) - new Date(firstCar.creationDate)
		);
	});

	return carsList.slice(0, CARS_IN_GRID);
};

export const getSimilarCarsList = (state) => (selectedCar) => {
	const similarCars = state.carsList.filter(
		(car) =>
			car.id !== selectedCar.id &&
			car.constructionType === selectedCar.constructionType &&
			car.wagonType === selectedCar.wagonType &&
			car.available
	);

	return similarCars.slice(0, CARS_IN_GRID);
};

export const isNewestCar = (state, getters) => (id) => {
	return !!getters.newestCarsList.find((car) => car.id === id);
};

export const carsPriceRange = (state) => {
	const carsList = [...state.carsList];

	carsList.sort((firstCar, secondCar) => {
		return firstCar.price - secondCar.price;
	});

	return {
		min: carsList[0].price,
		max: carsList[carsList.length - 1].price,
	};
};

export const filteredCars =
	(state) => (filterParams, sortType, searchString) => {
		const carsList = [...state.carsList];
		const {
			transmissionGroup,
			yearsGroup,
			constructionTypeGroup,
			priceRange,
		} = filterParams;

		const filtered = carsList.filter((car) => {
			const fullCarInfo = Object.values(car).join(' ').toLowerCase();

			return (
				car.price >= priceRange.min &&
				car.price <= priceRange.max &&
				(!transmissionGroup.length ||
					transmissionGroup.includes(car.transmissionType)) &&
				(!yearsGroup.length || yearsGroup.includes(car.year)) &&
				(!constructionTypeGroup.length ||
					constructionTypeGroup.includes(car.constructionType)) &&
				fullCarInfo.includes(searchString.toLowerCase())
			);
		});

		// Need sort filtered cars list exactly inside getter.
		// Otherwise we have incorrect car order after fade-move animation
		if (sortType.name === 'date') {
			filtered.sort((firstCar, secondCar) => {
				return (
					new Date(secondCar.creationDate) -
					new Date(firstCar.creationDate)
				);
			});
		} else {
			filtered.sort((firstCar, secondCar) => {
				return sortType.name === 'priceAsc'
					? firstCar.price - secondCar.price
					: secondCar.price - firstCar.price;
			});
		}

		return filtered;
	};
