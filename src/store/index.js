import { store } from 'quasar/wrappers';
import { createStore } from 'vuex';
import cars from './cars';

// import example from './module-example'

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default store((/* { ssrContext } */) => {
	const Store = createStore({
		modules: {
			cars,
		},

		// enable strict mode (adds overhead!)
		// for dev mode and --debug builds only

		// TODO
		strict: true,
	});

	return Store;
});
