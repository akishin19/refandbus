import { boot } from 'quasar/wrappers';
import { createI18n } from 'vue-i18n';
import messages from 'src/i18n';

function getPluralForms(quantity, choicesLength) {
	const lastDigit = quantity % 10;
	const lastTwoDigits = quantity % 100;

	if (choicesLength < 3) return quantity === 1 ? 0 : 1;
	if (lastDigit === 1 && lastTwoDigits !== 11) {
		return 0;
	}
	if (
		lastDigit >= 2 &&
		lastDigit <= 4 &&
		(lastTwoDigits < 10 || lastTwoDigits >= 20)
	) {
		return 1;
	}
	return 2;
}

export const i18n = createI18n({
	locale: 'uk',
	fallbackLocale: 'uk',
	globalInjection: true,
	warnHtmlMessage: false,
	pluralRules: {
		uk: getPluralForms,
	},
	messages,
});

export default boot(({ app }) => {
	// Set i18n instance on app
	app.use(i18n);
});
