import axios from 'axios';
import { boot } from 'quasar/wrappers';
import { getAppBaseUrl } from 'utils/helpers';

axios.defaults.baseURL = getAppBaseUrl();

axios.defaults.headers.common['Content-Type'] = 'application/json';

export default boot(({ app }) => {
	app.config.globalProperties.$axios = axios;
});
