import axios from 'axios';

/**
 * Send message about callback or test-drive requests via telegram bot
 * @param {Object} requestData
 * @param {String} requestData.name
 * @param {String} requestData.phone
 * @param {String} requestData.companyName
 * @param {String} requestData.date
 * @returns {Object}
 */
export function apiTelegramSendMessage(requestData) {
	return axios.post('/sendMessage/', requestData);
}
