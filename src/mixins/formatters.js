export default {
	data() {
		return {
			formatter: new Intl.NumberFormat('ru', {
				style: 'currency',
				currency: 'USD',
				minimumFractionDigits: 2,
			}),
		};
	},

	methods: {
		formatAmount(amount) {
			return this.formatter.format(amount);
		},
	},
};
