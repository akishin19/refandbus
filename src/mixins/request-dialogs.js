import { getClearNumber } from 'utils/formatters';

export default {
	data() {
		return {
			VALID_PHONE_LENGHT: 12,
		};
	},

	computed: {
		nameValidationText() {
			if (this.v$.form.name.required.$invalid) {
				return '"Прізвище та ім\'я" обов\'язкові';
			} else if (this.v$.form.name.maxLength.$invalid) {
				return '"Прізвище та ім\'я" занадто довгі';
			}

			return '"Прізвище та ім\'я" некоректні';
		},

		phoneValidationText() {
			if (this.v$.form.phone.required.$invalid) {
				return '"Телефон" обов\'язковий';
			} else if (this.v$.form.phone.minLength.$invalid) {
				return '"Телефон" занадто короткий';
			}

			return '"Телефон" некоректний';
		},
	},

	methods: {
		pastePhone(event) {
			event.preventDefault();

			this.form.phone = getClearNumber(event);

			this.$q.notify({
				message: 'Номер успішно вставлено',
				color: 'positive',
				icon: 'done',
				timeout: 2000,
				progress: true,
			});
		},
	},
};
