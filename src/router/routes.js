const routes = [
	{
		path: '/',
		name: 'home',
		component: () => import('layouts/MainLayout.vue'),
		children: [
			{
				path: '',
				name: 'main',
				component: () => import('pages/MainPage.vue'),
			},
			{
				path: 'cars',
				name: 'cars',
				component: () => import('pages/cars/CarsPageLayout.vue'),
				children: [
					{
						path: '',
						name: 'cars-list',
						component: () => import('pages/cars/CarsListPage.vue'),
					},
					{
						path: ':id',
						name: 'car',
						component: () => import('pages/cars/SingleCarPage.vue'),
						props: true,
					},
				],
			},
			{
				path: 'services',
				name: 'services',
				component: () => import('pages/ServicesPage.vue'),
			},
			{
				path: 'about-us',
				name: 'about',
				component: () => import('pages/AboutUsPage.vue'),
			},
			{
				path: 'payment-and-delivery',
				name: 'payment',
				component: () => import('pages/PaymentsAndDeliveryPage.vue'),
			},
			{
				path: 'clients',
				name: 'clients',
				component: () => import('pages/ClientsPage.vue'),
			},
			{
				path: 'contacts',
				name: 'contacts',
				component: () => import('pages/ContactsPage.vue'),
			},
			// Always leave this as last one, but you can also remove it
			{
				path: '/:catchAll(.*)*',
				name: 'error-not-found',
				component: () => import('pages/ErrorNotFound.vue'),
			},
		],
	},
];

export default routes;
