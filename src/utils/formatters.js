/**
 * Clean data from any non-digit characters
 * @param {Object|String} number - Native 'paste' event object or plain string
 * @returns {String}
 */
export function getClearNumber(number) {
	const dataToPaste = number.clipboardData?.getData('text') || number;

	return dataToPaste.replace(/\D/g, '');
}

/**
 * Format phone number to +XX (XXX) XXX - XX - XX
 * @param {String} number - Phone number
 * @returns {String}
 */
export function getFormattedPhoneNumber(number) {
	const match = number.match(/^(\d{2})(\d{3})(\d{3})(\d{2})(\d{2})$/);

	return `+${match[1]} (${match[2]}) ${match[3]} - ${match[4]} - ${match[5]}`;
}
