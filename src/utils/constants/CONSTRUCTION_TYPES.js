export default {
	'refrigerator-booth': {
		value: 'refrigerator-booth',
		label: 'Рефрижератор-будка',
	},
	broadside: {
		value: 'broadside',
		label: 'Бортова',
	},
	'refrigerator-wagon': {
		value: 'refrigerator-wagon',
		label: 'Рефрижератор-фургон',
	},
	passenger: {
		value: 'passenger',
		label: 'Пасажир',
	},
	booth: {
		value: 'booth',
		label: 'Будка',
	},
	'wagon-maxi-base': {
		value: 'wagon-maxi-base',
		label: 'Фургон максі-база',
	},
};
