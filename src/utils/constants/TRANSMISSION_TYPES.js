export default {
	'mechanical-six': {
		value: 'mechanical-six',
		label: 'Механіка 6-ст',
	},
	'seven-g-tronic': {
		value: 'seven-g-tronic',
		label: '7G-Tronic',
	},
	automatic: {
		value: 'automatic',
		label: 'Автомат',
	},
};
