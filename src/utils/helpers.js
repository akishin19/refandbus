/**
 * Loads images from the provided asynchronous image promises
 * @async
 * @param {Object} imagePromises - An object containing asynchronous image promises, where image paths are used
 * as keys and the promises resolve to modules with a 'default' property containing the image path.
 * @returns {Object}
 */
export async function loadImages(imagePromises) {
	return await Promise.all(
		Object.values(imagePromises).map(
			async (imagePromise) => (await imagePromise()).default
		)
	);
}

/**
 * Get application base url
 * @returns {String}
 */
export function getAppBaseUrl() {
	return process.env.PROD && !process.env.APP_TEST_MODE
		? `https://${process.env.APP_DOMAIN}`
		: `http://${process.env.APP_DOMAIN}:${process.env.APP_PORT}`;
}
