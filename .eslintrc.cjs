module.exports = {
	// https://eslint.org/docs/user-guide/configuring#configuration-cascading-and-hierarchy
	// This option interrupts the configuration hierarchy at this file
	// Remove this if you have an higher level ESLint config file (it usually happens into a monorepos)
	root: true,

	parserOptions: {
		ecmaVersion: 2021, // Allows for the parsing of modern ECMAScript features
	},

	env: {
		node: true,
		browser: true,
		'vue/setup-compiler-macros': true,
	},

	// Rules order is important, please avoid shuffling them
	extends: [
		// Base ESLint recommended rules
		// 'eslint:recommended',

		// Uncomment any of the lines below to choose desired strictness,
		// but leave only one uncommented!
		// See https://eslint.vuejs.org/rules/#available-rules
		'plugin:vue/vue3-essential', // Priority A: Essential (Error Prevention)
		// 'plugin:vue/vue3-strongly-recommended', // Priority B: Strongly Recommended (Improving Readability)
		// 'plugin:vue/vue3-recommended', // Priority C: Recommended (Minimizing Arbitrary Choices and Cognitive Overhead)

		// https://github.com/prettier/eslint-config-prettier#installation
		// usage with Prettier, provided by 'eslint-config-prettier'.
		'prettier',
	],

	plugins: [
		// https://eslint.vuejs.org/user-guide/#why-doesn-t-it-work-on-vue-files
		// required to lint *.vue files
		'vue',

		// https://github.com/typescript-eslint/typescript-eslint/issues/389#issuecomment-509292674
		// Prettier has not been included as plugin to avoid performance impact
		// add it as an extension for your IDE
	],

	globals: {
		ga: 'readonly', // Google Analytics
		cordova: 'readonly',
		__statics: 'readonly',
		__QUASAR_SSR__: 'readonly',
		__QUASAR_SSR_SERVER__: 'readonly',
		__QUASAR_SSR_CLIENT__: 'readonly',
		__QUASAR_SSR_PWA__: 'readonly',
		process: 'readonly',
		Capacitor: 'readonly',
		chrome: 'readonly',
	},

	// add your custom rules here
	rules: {
		'prefer-promise-reject-errors': 'off',

		'no-param-reassign': 0,

		'import/first': 0,
		'import/named': 0,
		'import/namespace': 0,
		'import/default': 0,
		'import/export': 0,
		'import/extensions': 0,
		'import/no-unresolved': 0,
		'import/no-extraneous-dependencies': 0,

		indent: [1, 'tab', { SwitchCase: 1 }],
		'no-tabs': 0,
		'vue/html-indent': [
			'warn',
			'tab',
			{
				attribute: 1,
				closeBracket: 0,
				alignAttributesVertically: true,
				ignores: [],
			},
		],
		'vue/max-attributes-per-line': [
			2,
			{
				singleline: 3,
				multiline: {
					max: 3,
				},
			},
		],
		'vue/prop-name-casing': [2, 'camelCase'],
		'vue/attribute-hyphenation': 0,
		camelcase: [2, { properties: 'never' }],
		'max-len': ['error', { code: 200, ignoreComments: true }],
		'no-underscore-dangle': ['error', { allow: ['_chart'] }],
		'no-plusplus': 0,
		'no-bitwise': 0,
		'no-lonely-if': 0,
		'object-curly-newline': [
			'error',
			{
				ObjectExpression: { consistent: true },
				ObjectPattern: { consistent: true },
				ImportDeclaration: { multiline: true, minProperties: 5 },
				ExportDeclaration: { multiline: true },
			},
		],
		'no-else-return': ['error', { allowElseIf: true }],
		'no-trailing-spaces': 1,

		'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
		// allow debugger during development only
		'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
	},
};
